package com.example.memegenie;

public class ExampleItem {
    private String mImageUrl;
    private String mCreator;
    private int mCount;

    public ExampleItem(String imageUrl, String creator, int count) {
        mImageUrl = imageUrl;
        mCreator = creator;
        mCount = count;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getCreator() {
        return mCreator;
    }

    public int getCount(){return mCount;}
}
