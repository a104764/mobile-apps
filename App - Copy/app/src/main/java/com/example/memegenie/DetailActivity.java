package com.example.memegenie;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import static com.example.memegenie.CreateActivity.EXTRA_COUNT;
import static com.example.memegenie.CreateActivity.EXTRA_NAME;
import static com.example.memegenie.CreateActivity.EXTRA_URL;

public class DetailActivity extends AppCompatActivity{

    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        LinearLayout mLinearLayout = new LinearLayout(this);
        mLinearLayout = (LinearLayout)findViewById(R.id.linearLayout_detail);


        Intent intent = getIntent();
        String imageURL = intent.getStringExtra(EXTRA_URL);
        String memeName = intent.getStringExtra(EXTRA_NAME);
        int count = intent.getIntExtra(EXTRA_COUNT, 0);

        ImageView imageView = findViewById(R.id.image_view_detail);
        TextView textViewName = findViewById(R.id.text_view_name_detail);
        Button btn = findViewById(R.id.button_create);


        Picasso.with(this).load(imageURL).fit().centerInside().into(imageView);
        textViewName.setText(memeName);

        EditText lEditText;

        for(int i = 1; i <= count;i++){
            lEditText = new EditText(this);
            lEditText .setLayoutParams(
                    new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT,
                            LayoutParams.WRAP_CONTENT));
            lEditText.setHint("Caption" + i);
            mLinearLayout.addView(lEditText);

        }

    }



}
