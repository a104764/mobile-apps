package com.example.memegenie;

import android.content.SharedPreferences;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import java.text.DateFormat;
import java.util.Date;

public class NoteActivity extends AppCompatActivity {

    private TextView textView;
    private EditText editText;


    private CoordinatorLayout coordinatorLayout;

    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String TEXT = "text";
    public static final String DATE = "date";


    private String text;
    private String date;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        coordinatorLayout = findViewById(R.id.coordinator_Layout);




        textView = (TextView) findViewById(R.id.textview);
        editText = (EditText) findViewById(R.id.edittext);



    }

    public void showSnackbar(String d) {
        Snackbar snackbar = Snackbar.make(coordinatorLayout, d, Snackbar.LENGTH_INDEFINITE);
        snackbar.show();
    }

    @Override
    protected  void onStart() {

        super.onStart();

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        text = sharedPreferences.getString(TEXT, "");
        date = sharedPreferences.getString(DATE, "");

        updateViews();
    }

    @Override
    protected void onResume() {

        super.onResume();

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        text = sharedPreferences.getString(TEXT, "");
        date = sharedPreferences.getString(DATE, "");

        updateViews();
    }

    @Override
    protected void onStop() {

        super.onStop();

        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(TEXT, editText.getText().toString());
        editor.putString(DATE, currentDateTimeString);

        editor.apply();
    }

    @Override
    protected void onPause() {

        super.onPause();

        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());

        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(TEXT, editText.getText().toString());
        editor.putString(DATE, currentDateTimeString);

        editor.apply();
    }


    public void updateViews() {
        textView.setText(text);
        showSnackbar(date);
    }



}
