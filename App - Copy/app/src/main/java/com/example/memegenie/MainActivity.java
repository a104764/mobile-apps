package com.example.memegenie;

import android.Manifest;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.goodiebag.pinview.Pinview;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.ActionCodeSettings;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int MY_REQUEST_CODE = 123;
    List<AuthUI.IdpConfig> providers;
    Button btn_sign_out;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        FirebaseAuth auth = FirebaseAuth.getInstance();



        //Init providers
        providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(), //email Builder
                new AuthUI.IdpConfig.PhoneBuilder().build(), //phone Builder
                new AuthUI.IdpConfig.FacebookBuilder().build(), //fb Builder
                new AuthUI.IdpConfig.GoogleBuilder().build()//google Builder

        );

        showSignInOptins();


    }

    private void showSignInOptins() {

        startActivityForResult(
                AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(providers)
                .setTheme(R.style.MyTeam)
                .build(),MY_REQUEST_CODE
        );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MY_REQUEST_CODE){
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if(resultCode == RESULT_OK){
                //Get user
                //FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                //Show email on toast
                //Toast.makeText(this, ""+user.getEmail(),Toast.LENGTH_SHORT).show();
                //set Button signout
                //btn_sign_out.setEnabled(true);
                finish();
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
            }
            else{
                Toast.makeText(this, ""+response.getError(),Toast.LENGTH_SHORT).show();

            }
        }
    }
}








